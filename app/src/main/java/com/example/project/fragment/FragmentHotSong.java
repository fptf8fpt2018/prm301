package com.example.project.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.adapter.HotSongAdapter;
import com.example.project.models.SingSong;
import com.example.project.service.APIService;
import com.example.project.service.DataService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentHotSong extends Fragment {

    View view;

    RecyclerView recyclerViewHotSong;

    HotSongAdapter hotSongAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull  LayoutInflater inflater,
                             @Nullable  ViewGroup container,
                             @Nullable  Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_hot_song, container, false);

        mapView();
        getViewData();

        return view;
    }

    private void mapView() {
        recyclerViewHotSong = view.findViewById(R.id.recyler_view_hot_songs);
    }

    private void getViewData() {
        DataService dataService = APIService.getService();
        Call<List<SingSong>> callback  = dataService.select5hotSong();
        callback.enqueue(new Callback<List<SingSong>>() {
            @Override
            public void onResponse(Call<List<SingSong>> call, Response<List<SingSong>> response) {
                List<SingSong> songs = response.body();

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
                recyclerViewHotSong.setLayoutManager(linearLayoutManager);

                hotSongAdapter = new HotSongAdapter(getActivity(), songs);
                recyclerViewHotSong.setAdapter(hotSongAdapter);

                //Log.d("bbb", songs.get(0).getSingSongName());
            }

            @Override
            public void onFailure(Call<List<SingSong>> call, Throwable t) {
                Log.d("bbb", "Failed");
            }
        });
    }
}
