package com.example.project.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.adapter.SongSearchAdapter;
import com.example.project.models.SingSong;
import com.example.project.service.APIService;
import com.example.project.service.DataService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentSearch extends Fragment {

    View view;
    Toolbar toolbarSearchSong;
    RecyclerView recyclerViewSearchSong;
    TextView tvSearchSongNotFound;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);

        mapView();

        initHardcodeToolbar();

        setHasOptionsMenu(true);

        return view;
    }



    private void mapView() {
        toolbarSearchSong = view.findViewById(R.id.toolbar_search_song);
        recyclerViewSearchSong = view.findViewById(R.id.recyler_view_search_song);
        tvSearchSongNotFound = view.findViewById(R.id.tv_search_song_not_found);
    }

    private void initHardcodeToolbar() {
        //getActivity tu fragment thanh activity
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbarSearchSong);
        toolbarSearchSong.setTitle("");
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_view, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            //bam nut search
            @Override
            public boolean onQueryTextSubmit(String keyword) {
                search(keyword);

                //Log.d("bbb", "search view onQueryTextSubmit: keyword " + query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                search(newText);

                return true;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void search(String keyword){
        DataService dataService = APIService.getService();
        Call<List<SingSong>> callback = dataService.selectSongsByIncluedNamm(keyword);
        callback.enqueue(new Callback<List<SingSong>>() {
            @Override
            public void onResponse(Call<List<SingSong>> call, Response<List<SingSong>> response) {
                List<SingSong> searchedSong = response.body();

                if(searchedSong != null && searchedSong.size() > 0){
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                    recyclerViewSearchSong.setLayoutManager(linearLayoutManager);

                    SongSearchAdapter adapter = new SongSearchAdapter(getActivity(), searchedSong);
                    recyclerViewSearchSong.setAdapter(adapter);

                    tvSearchSongNotFound.setVisibility(View.GONE);
                    recyclerViewSearchSong.setVisibility(View.VISIBLE);
                }else{
                    tvSearchSongNotFound.setVisibility(View.VISIBLE);
                    recyclerViewSearchSong.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<List<SingSong>> call, Throwable t) {
                Log.d("bb", "failed at FragmentSearch.java");
            }
        });
    }
}
