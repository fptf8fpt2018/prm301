package com.example.project.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.activity.AllAlbumsActivity;
import com.example.project.activity.AllTopicActivity;
import com.example.project.activity.SongListActivity;
import com.example.project.adapter.AlbumAdapter;
import com.example.project.models.Album;
import com.example.project.service.APIService;
import com.example.project.service.DataService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentAlbum extends Fragment {


    View view;


    RecyclerView recyclerViewAlbums;
    TextView tvSeeMore;

    AlbumAdapter albumAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull  LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_album, container, false);

        mapView();

        setEvent();

        getViewData();

        return  view;
    }

    private void setEvent() {
        tvSeeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AllAlbumsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void mapView(){
        recyclerViewAlbums = view.findViewById(R.id.recyler_view_all_albums);
        tvSeeMore = view.findViewById(R.id.tv_see_more_albums);
    }

    private void getViewData() {
        DataService dataService = APIService.getService();
        Call<List<Album>> callback = dataService.select4newestAlbums();

        callback.enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                List<Album> albums = response.body();
                albums.addAll(albums);
                albums.addAll(albums);

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                recyclerViewAlbums.setLayoutManager(linearLayoutManager);

                albumAdapter = new AlbumAdapter(getActivity(), albums);
                recyclerViewAlbums.setAdapter(albumAdapter);

                //Log.d("bb",  albums.get(0).getAlbumName());
            }

            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {
                Log.d("bb", "Failed");
            }
        });


    }
}
