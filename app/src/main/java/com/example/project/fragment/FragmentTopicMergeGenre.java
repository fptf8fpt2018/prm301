package com.example.project.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.LongDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.project.R;
import com.example.project.activity.AllTopicActivity;
import com.example.project.activity.GenreListByTopicActivity;
import com.example.project.activity.SongListActivity;
import com.example.project.models.Genre;
import com.example.project.models.Topic;
import com.example.project.models.TopicMergeGenre;
import com.example.project.service.APIService;
import com.example.project.service.DataService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentTopicMergeGenre extends Fragment {

    View view;

    HorizontalScrollView horizontalScrollView;
    TextView tvSeeMore;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_topic_merge_genre, container, false);
        map();
        setEvent();
        getData();
        return view;
    }


    private void map() {
        horizontalScrollView = view.findViewById(R.id.horizontalScrollView);
        tvSeeMore = view.findViewById(R.id.tv_see_more_topic_and_genre);
    }

    private void setEvent() {
        tvSeeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AllTopicActivity.class);
                startActivity(intent);
            }
        });
    }


    private void getData() {
        DataService dataService = APIService.getService();
        Call<TopicMergeGenre> callback = dataService.select8newestTopicAndGenre();

        callback.enqueue(new Callback<TopicMergeGenre>() {
            @Override
            public void onResponse(Call<TopicMergeGenre> call, Response<TopicMergeGenre> response) {
                TopicMergeGenre topicMergeGenre = response.body();

                final List<Topic> topics = new ArrayList<>();
                topics.addAll(topicMergeGenre.getTopics());
                topics.addAll(topicMergeGenre.getTopics());
                topics.addAll(topicMergeGenre.getTopics());
                topics.addAll(topicMergeGenre.getTopics());


                final List<Genre> genres = new ArrayList<>();
                genres.addAll(topicMergeGenre.getGenres());
                genres.addAll(topicMergeGenre.getGenres());
                genres.addAll(topicMergeGenre.getGenres());
                genres.addAll(topicMergeGenre.getGenres());


                LinearLayout linearLayout = new LinearLayout(getActivity());
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(580, 250);
                layoutParams.setMargins(10,20,10,20);

                //tranverse array topic
                for(int i = 0; i < topics.size(); i ++){
                    //bo lai
                    CardView cardView = new CardView(getActivity());
                    cardView.setRadius(10);
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);

                    //check exist data(image)
                    String curTopicImage = topics.get(i).getTopicImagine();
                    if(curTopicImage != null){
                        Picasso.with(getActivity()).load(curTopicImage).into(imageView);
                    }

                    cardView.setLayoutParams(layoutParams);
                    cardView.addView(imageView);
                    linearLayout.addView(cardView);

                    //event
                    int finalI = i;
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), GenreListByTopicActivity.class);
                            intent.putExtra("topic", topics.get(finalI));
                            startActivity(intent);
                        }
                    });
                }

                //tranverse array genre
                for(int i = 0; i < genres.size(); i ++){
                    //bo lai
                    CardView cardView = new CardView(getActivity());
                    cardView.setRadius(10);
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);

                    //check exist data(image)
                    String curTopicImage = genres.get(i).getGenreImagine();
                    if(curTopicImage != null){
                        Picasso.with(getActivity()).load(curTopicImage).into(imageView);
                    }

                    cardView.setLayoutParams(layoutParams);
                    cardView.addView(imageView);
                    linearLayout.addView(cardView);

                    //event
                    int finalI = i;
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent  = new Intent(getActivity(), SongListActivity.class);
                            intent.putExtra("genre", genres.get(finalI));

                            getContext().startActivity(intent);
                        }
                    });
                }

                horizontalScrollView.addView(linearLayout);

                //Log.d("bbb", topicMergeGenre.getGenres().get(0).getGenreName() );
            }

            @Override
            public void onFailure(Call<TopicMergeGenre> call, Throwable t) {
                Log.d("bbb", "Failed at FragmentTopicMergeGenre.java");
            }
        });


    }
}
