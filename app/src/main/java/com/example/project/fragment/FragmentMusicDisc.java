package com.example.project.fragment;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.project.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentMusicDisc extends Fragment {

    View view;
    CircleImageView circleImageViewMusicDisc;
    ObjectAnimator objectAnimator; //click vao tao hinh anh

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_music_disc, container, false);

        mapView();
        setEvent();

        return view;
    }


    private void mapView() {
        circleImageViewMusicDisc = view.findViewById(R.id.circle_imgv_music_disc);
    }

    private void setEvent() {
        objectAnimator
                = ObjectAnimator.ofFloat(circleImageViewMusicDisc, "rotation", 0f, 360f);
        objectAnimator.setDuration(10000);
        objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
        objectAnimator.setRepeatMode(ValueAnimator.RESTART);
        objectAnimator.setInterpolator(new LinearInterpolator());//khong bi khung khi quay het 1 vong
    }


    public void setImg(String imgUrl) {
        Picasso.with(getActivity()).load(imgUrl).into(circleImageViewMusicDisc);
    }

}
