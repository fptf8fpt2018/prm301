package com.example.project.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.project.R;
import com.example.project.adapter.BannerAdapter;
import com.example.project.models.Advertisement;
import com.example.project.service.APIService;
import com.example.project.service.DataService;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentBanner extends Fragment {

    View view;

    ViewPager viewPager;
    CircleIndicator circleIndicator;

    BannerAdapter bannerAdapter;

    Runnable runnable;
    Handler handler;
    int currentItem;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable  ViewGroup container,
                             @Nullable  Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_banner, container, false);

        map();


        getData();
        return  view;
    }

    private void map() {
        viewPager = view.findViewById(R.id.view_pager);
        circleIndicator = view.findViewById(R.id.default_circle_indicator);
    }


    private void getData() {
        DataService dataService = APIService.getService();

        //function tra ve
        Call<List<Advertisement>> callback = dataService.selectSongBanner();
        callback.enqueue(new Callback<List<Advertisement>>() {
            @Override
            public void onResponse(Call<List<Advertisement>> call, Response<List<Advertisement>> response) {
                List<Advertisement> banners = (List<Advertisement>) response.body();

                List<Advertisement> bannersx3 = new ArrayList<>(banners);
                bannersx3.addAll(banners);
                bannersx3.addAll(banners);

                bannerAdapter = new BannerAdapter(getActivity(), bannersx3);
                viewPager.setAdapter(bannerAdapter);
                circleIndicator.setViewPager(viewPager);

                //chuyen banner( include indicator) auto sau 4,5s
                handler = new Handler();
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        currentItem = viewPager.getCurrentItem();
                        currentItem++;

                        //last banner
                        if(currentItem >= viewPager.getAdapter().getCount()){
                            currentItem = 0;
                        }

                        viewPager.setCurrentItem(currentItem, true);
                        handler.postDelayed(runnable,4500);
                    }
                };
                handler.postDelayed(runnable, 4500);

                //Log.d("bb", banners.get(0).getSongName());
            }

            @Override
            public void onFailure(Call<List<Advertisement>> call, Throwable t) {
                Log.d("bb", "failed at FragmentAdapter");
            }
        });
    }
}
