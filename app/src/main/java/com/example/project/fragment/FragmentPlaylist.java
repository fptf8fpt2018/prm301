package com.example.project.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.project.R;
import com.example.project.activity.AllPlaylistActivity;
import com.example.project.activity.SongListActivity;
import com.example.project.adapter.PlaylistAdapter;
import com.example.project.models.Playlist;
import com.example.project.service.APIRetrofitClient;
import com.example.project.service.APIService;
import com.example.project.service.DataService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentPlaylist extends Fragment {

    View view;

    ListView listViewPlaylist;
    TextView tvPlaylistIntro;
    TextView tvSeeMorePlaylist;

    PlaylistAdapter playlistAdapter;
    List<Playlist> playlists;

    @Nullable
    @Override
    public View onCreateView(@NonNull  LayoutInflater inflater,
                             @Nullable  ViewGroup container,
                             @Nullable  Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_playlist, container, false);

        map();

        setEvent();

        getData();

        return view;
    }

    private void map() {
        listViewPlaylist = view.findViewById(R.id.lstv_playlist);
        tvPlaylistIntro = view.findViewById(R.id.tv_playlist_intro);
        tvSeeMorePlaylist = view.findViewById(R.id.tv_more_playlist);
    }


    private void setEvent() {
        //event click display more
        tvSeeMorePlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AllPlaylistActivity.class);
                startActivity(intent);
            }
        });
    }


    private void getData() {
        DataService dataService = APIService.getService();
        Call<List<Playlist>> callback = dataService.select3newestPlaylist();

        callback.enqueue(new Callback<List<Playlist>>() {
            @Override
            public void onResponse(Call<List<Playlist>> call, Response<List<Playlist>> response) {
                playlists = response.body();
                playlists.addAll(playlists);
                playlists.addAll(playlists);

                playlistAdapter = new PlaylistAdapter(getActivity(), android.R.layout.simple_list_item_1, playlists);

                listViewPlaylist.setAdapter(playlistAdapter);
                setListViewHeightBasedOnChildren(listViewPlaylist);

                //event click item
                listViewPlaylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(getActivity(), SongListActivity.class);
                        intent.putExtra("playlist", playlists.get(position));
                        startActivity(intent);
                    }
                });


                //Log.d("bbb", playlists.get(0).getPlaylistName());
            }

            @Override
            public void onFailure(Call<List<Playlist>> call, Throwable t) {
                //Log.d("bbb", "failed");
            }
        });
    }

    //ref android listview measure height
    //https://stackoverflow.com/questions/21620764/android-listview-measure-height
    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);

            if(listItem != null){
                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
                listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();

            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
