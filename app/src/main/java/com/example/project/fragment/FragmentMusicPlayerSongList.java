package com.example.project.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.activity.MusicPlayerActivity;
import com.example.project.adapter.MusicPlayerAdapter;
import com.example.project.models.SingSong;

import java.util.List;

public class FragmentMusicPlayerSongList extends Fragment {

    View view;
    RecyclerView recyclerViewMusicPlayer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_music_player_song_list, container, false);

        mapView();

        //lay du lieu tu activity de hien thi
        List<SingSong> songs = MusicPlayerActivity.finalSongs;
        if(songs != null && songs.size() > 0){
            LinearLayoutManager manager = new LinearLayoutManager(getActivity());
            recyclerViewMusicPlayer.setLayoutManager(manager);

            MusicPlayerAdapter adapter = new MusicPlayerAdapter(getActivity(), MusicPlayerActivity.finalSongs);
            recyclerViewMusicPlayer.setAdapter(adapter);
        }

        return view;
    }

    private void mapView() {
        recyclerViewMusicPlayer = view.findViewById(R.id.recyler_view_music_player);
    }
}
