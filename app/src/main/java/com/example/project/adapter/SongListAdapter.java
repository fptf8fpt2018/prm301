package com.example.project.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.activity.MusicPlayerActivity;
import com.example.project.models.SingSong;
import com.example.project.service.APIService;
import com.example.project.service.DataService;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SongListAdapter extends RecyclerView.Adapter<SongListAdapter.ViewHolder> {

    Context context;
    List<SingSong> songs;

    public SongListAdapter(Context context, List<SingSong> songs) {
        this.context = context;
        this.songs = songs;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.song_list_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SongListAdapter.ViewHolder holder, int position) {
        SingSong song = songs.get(position);

        holder.tvSongIndex.setText( String.valueOf(position + 1) );
        holder.tvSongSinger.setText(song.getSingSongSinger());
        holder.tvSongName.setText(song.getSingSongName());

    }

    @Override
    public int getItemCount() {
        return songs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView tvSongIndex;
        TextView tvSongName;
        TextView tvSongSinger;
        ImageView imgvLike;

        public ViewHolder(@NonNull  View itemView) {
            super(itemView);
            tvSongIndex = itemView.findViewById(R.id.tv_song_list_item_index);
            tvSongName = itemView.findViewById(R.id.tv_song_list_item_name);
            tvSongSinger = itemView.findViewById(R.id.tv_song_list_item_singer);
            imgvLike =  itemView.findViewById(R.id.imgv_song_list_item_image_like);


            //event
            imgvLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imgvLike.setImageResource(R.drawable.iconloved);
                    imgvLike.setEnabled(false);

                    DataService dataService = APIService.getService();
                    String songId = songs.get(getPosition()).getSingSongId();
                    Call<String> callback = dataService.updateLike(songId, "1");

                    callback.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            String result = response.body();
                            //like successful
                            if(result.equals("1")){
                                //Log.d("bbb", "Loved" );
                                //Toast.makeText(context, "Loved", Toast.LENGTH_LONG).show();
                            }else{
                                Log.d("bbb", "Error at HotSongAdapter / constructor ViewHolder" );
                                //Toast.makeText(context, "Error !!!", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {

                        }
                    });

                    //Log.d("bbb", hotSongs.get(getPosition()).getSingSongName());
                }
            });

            //event click song
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MusicPlayerActivity.class);
                    SingSong song =  songs.get(getPosition());
                    intent.putExtra("song", song);
                    context.startActivity(intent);
                }
            });
        }
    }
}
