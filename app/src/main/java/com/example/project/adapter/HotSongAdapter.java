package com.example.project.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.activity.MusicPlayerActivity;
import com.example.project.models.SingSong;
import com.example.project.service.APIService;
import com.example.project.service.DataService;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.zip.Inflater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HotSongAdapter extends RecyclerView.Adapter<HotSongAdapter.ViewHolder> {

    Context context;
    List<SingSong> hotSongs;



    public HotSongAdapter(Context context, List<SingSong> hotSongs) {
        this.context = context;
        this.hotSongs = hotSongs;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view = layoutInflater.inflate(R.layout.hot_song_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull  HotSongAdapter.ViewHolder holder, int position) {
        SingSong hotSong = hotSongs.get(position);

        Picasso.with(context).load(hotSong.getSingSongImage()).into(holder.imgvHotSong);
        holder.tvHotSongName.setText(hotSong.getSingSongName());
        holder.tvHotSongSinger.setText(hotSong.getSingSongSinger());

    }

    @Override
    public int getItemCount() {
        return hotSongs.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvHotSongName;
        TextView tvHotSongSinger;
        ImageView imgvHotSong;
        ImageView imgvHotSongTotalLike;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvHotSongName = itemView.findViewById(R.id.tv_hot_song_name);
            tvHotSongSinger = itemView.findViewById(R.id.tv_hot_song_singer);

            imgvHotSong = itemView.findViewById(R.id.imgv_hot_song);
            imgvHotSongTotalLike = itemView.findViewById(R.id.imgv_hot_song_total_like);

            //event like
            imgvHotSongTotalLike.setOnClickListener(v -> {
                imgvHotSongTotalLike.setImageResource(R.drawable.iconloved);
                imgvHotSongTotalLike.setEnabled(false);

                DataService dataService = APIService.getService();
                String songId = hotSongs.get(getPosition()).getSingSongId();
                String totalLikePerTime = "1";
                Call<String> callback = dataService.updateLike(songId, totalLikePerTime);

                callback.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        String result = response.body();
                        //like successful
                        if(result.equals("1")){
                            //Log.d("bbb", "Loved" );
                            //Toast.makeText(context, "Loved", Toast.LENGTH_LONG).show();
                        }else{
                            Log.d("bbb", "Error at HotSongAdapter / constructor ViewHolder" );
                            //Toast.makeText(context, "Error !!!", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d("bbb", "Error at HotSongAdapter / constructor ViewHolder" );
                    }
                });

                //Log.d("bbb", hotSongs.get(getPosition()).getSingSongName());
            });


            //event click song
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent  intent = new Intent(context, MusicPlayerActivity.class);
                    SingSong song =  hotSongs.get(getPosition());
                    intent.putExtra("song", song);
                    context.startActivity(intent);
                }
            });
        }
    }
}
