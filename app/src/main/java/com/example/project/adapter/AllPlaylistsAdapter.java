package com.example.project.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.activity.AllPlaylistActivity;
import com.example.project.activity.SongListActivity;
import com.example.project.models.Playlist;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AllPlaylistsAdapter extends RecyclerView.Adapter<AllPlaylistsAdapter.ViewHolder> {

    Context context;
    List<Playlist> playlists;

    public AllPlaylistsAdapter(Context context, List<Playlist> playlists) {
        this.context = context;
        this.playlists = playlists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view =  layoutInflater.inflate(R.layout.all_playlists_each_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull  AllPlaylistsAdapter.ViewHolder holder, int position) {
        Playlist playlist = playlists.get(position);
        Picasso.with(context).load(playlist.getPlaylistImagine()).into(holder.imgvPlaylistImage);
        holder.tvPlaylistName.setText(playlist.getPlaylistName());
    }


    @Override
    public int getItemCount() {
        return playlists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imgvPlaylistImage;
        TextView tvPlaylistName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgvPlaylistImage = itemView.findViewById(R.id.imgv_all_playlists_each_row_img);
            tvPlaylistName = itemView.findViewById(R.id.tv_all_playlist_each_row_name);

            //event click to see detail
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent  = new Intent(context, SongListActivity.class);
                    intent.putExtra("playlist", playlists.get(getPosition()));
                    context.startActivity(intent);

                }
            });
        }
    }
}
