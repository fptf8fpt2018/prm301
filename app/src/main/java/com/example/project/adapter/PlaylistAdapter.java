package com.example.project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.project.R;
import com.example.project.models.Playlist;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PlaylistAdapter extends ArrayAdapter<Playlist> {

    public PlaylistAdapter(@NonNull Context context, int resource, @NonNull List<Playlist> objects) {
        super(context, resource, objects);
    }


    class ViewHolder{
        TextView tvPlaylistName;
        ImageView imgvPlaylistBackground;
        ImageView imgvPlaylistIcon;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder = null;



        //first time
        if(convertView == null){
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            convertView = layoutInflater.inflate(R.layout.playlist_row, null);

            viewHolder = new ViewHolder();
            viewHolder.tvPlaylistName = convertView.findViewById(R.id.tv_playlist_name);
            viewHolder.imgvPlaylistBackground = convertView.findViewById(R.id.imgv_playlist_background);
            viewHolder.imgvPlaylistIcon = convertView.findViewById(R.id.imgv_playlist_icon);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = ( ViewHolder) convertView.getTag();
        }

        Playlist playlist = getItem(position);
        Picasso.with(getContext()).load(playlist.getPlaylistImagine()).into(viewHolder.imgvPlaylistBackground);
        Picasso.with(getContext()).load(playlist.getPlaylistIcon()).into(viewHolder.imgvPlaylistIcon);
        viewHolder.tvPlaylistName.setText(playlist.getPlaylistName());

        return convertView;
    }
}
