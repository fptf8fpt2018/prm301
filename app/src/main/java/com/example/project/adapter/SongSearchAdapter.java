package com.example.project.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.activity.MusicPlayerActivity;
import com.example.project.models.SingSong;
import com.example.project.service.APIService;
import com.example.project.service.DataService;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SongSearchAdapter extends RecyclerView.Adapter<SongSearchAdapter.ViewHolder> {

    Context context;
    List<SingSong> songs;

    public SongSearchAdapter(Context context, List<SingSong> songs) {
        this.context = context;
        this.songs = songs;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.song_search_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SongSearchAdapter.ViewHolder holder, int position) {
        SingSong song = songs.get(position);

        holder.tvSongSinger.setText(song.getSingSongSinger());
        holder.tvSongName.setText(song.getSingSongName());
        Picasso.with(context).load(song.getSingSongImage()).into(holder.imgvSongImg);
    }

    @Override
    public int getItemCount() {
        return songs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView tvSongName, tvSongSinger;
        ImageView imgvSongImg, imgvSongLike;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvSongName = itemView.findViewById(R.id.tv_song_search_item_name);
            tvSongSinger = itemView.findViewById(R.id.tv_song_search_item_singer);
            imgvSongImg = itemView.findViewById(R.id.imgv_song_search_item_img);
            imgvSongLike = itemView.findViewById(R.id.imgv_song_search_item_like);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MusicPlayerActivity.class);
                    intent.putExtra("song", songs.get(getPosition()));
                    context.startActivity(intent);
                }
            });


            imgvSongLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imgvSongLike.setImageResource(R.drawable.iconloved);
                    imgvSongLike.setEnabled(false);

                    DataService dataService = APIService.getService();
                    String songId = songs.get(getPosition()).getSingSongId();
                    String likePerTime = "1";
                    Call<String> callback = dataService.updateLike(songId, likePerTime);

                    callback.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            String result = response.body();
                            if(result.equals("1")){
                                //Log.d("bbb", "Loved" );
                                //Toast.makeText(context, "Loved", Toast.LENGTH_LONG).show();
                            }else{
                                Log.d("bbb", "Error at HotSongAdapter / constructor ViewHolder" );
                                //Toast.makeText(context, "Error !!!", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Log.d("bbb", "Error at SearchSongAdapter / constructor ViewHolder" );
                        }
                    });
                }
            });
        }
    }
}
