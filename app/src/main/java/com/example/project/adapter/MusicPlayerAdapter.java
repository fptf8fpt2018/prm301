package com.example.project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.models.SingSong;

import java.util.List;

public class MusicPlayerAdapter extends RecyclerView.Adapter<MusicPlayerAdapter.ViewHolder> {
    private Context context;
    private List<SingSong> songs;

    public MusicPlayerAdapter(Context context, List<SingSong> songs) {
        this.context = context;
        this.songs = songs;
    }

    @NonNull
    @Override
    public MusicPlayerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.music_player_song_list_each_row, parent, false);
        return new MusicPlayerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MusicPlayerAdapter.ViewHolder holder, int position) {
        SingSong song = songs.get(position);

        holder.textViewIndex.setText(String.valueOf(position + 1));
        holder.textViewSongName.setText(song.getSingSongName());
        holder.textViewSinger.setText(song.getSingSongSinger());
    }

    @Override
    public int getItemCount() {
        return songs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView textViewIndex, textViewSinger, textViewSongName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewIndex =itemView.findViewById(R.id.tv_music_player_item_index);
            textViewSinger = itemView.findViewById(R.id.tv_music_player_item_singer_name);
            textViewSongName = itemView.findViewById(R.id.tv_music_player_item_song_name);
        }
    }
}
