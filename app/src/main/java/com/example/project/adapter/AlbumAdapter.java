package com.example.project.adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.activity.SongListActivity;
import com.example.project.models.Album;
import com.squareup.picasso.Picasso;

import java.util.List;

//each row
public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {

    Context context;
    List<Album> albums;

    public AlbumAdapter(Context context, List<Album> albums) {
        this.context = context;
        this.albums = albums;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull  ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.album_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumAdapter.ViewHolder holder, int position) {
        Album album = albums.get(position);
        Picasso.with(context).load(album.getAlbumImage()).into(holder.imgvAlbumImage);
        holder.tvAlbumSingerName.setText(album.getAlbumName());
        holder.tvAlbumName.setText(album.getAlbumName());
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imgvAlbumImage;
        TextView tvAlbumName;
        TextView tvAlbumSingerName;

        public ViewHolder(@NonNull  View itemView) {
            super(itemView);
            imgvAlbumImage = itemView.findViewById(R.id.imgv_album_image);
            tvAlbumName = itemView.findViewById(R.id.tv_album_name);
            tvAlbumSingerName = itemView.findViewById(R.id.tv_ablum_singer_name);

            //event
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SongListActivity.class);
                    intent.putExtra("album", albums.get(getPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
