package com.example.project.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.project.R;
import com.example.project.activity.SongListActivity;
import com.example.project.models.Advertisement;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BannerAdapter extends PagerAdapter {

    Context context;
    List<Advertisement> banners;

    public BannerAdapter(Context context, List<Advertisement> banners) {
        this.context = context;
        this.banners = banners;
    }

    @Override
    public int getCount() {
        return banners.size();
    }

    //tra ve cai view hien thi
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    //dinh hinh du lieu cho moi object(view)
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.banner_row, null);


        Advertisement currBanner = banners.get(position);


        ImageView imgvBackgroundBanner = view.findViewById(R.id.imgv_background_banner);
        Picasso.with(context).load(currBanner.getAdsImagine()).into(imgvBackgroundBanner);

        ImageView imgvBanner = view.findViewById(R.id.imgv_banner);
        Picasso.with(context).load(currBanner.getSongImagine()).into(imgvBanner);

        TextView tvSongName = view.findViewById(R.id.tv_song_name);
        tvSongName.setText(currBanner.getSongName());

        TextView tvAdsContent = view.findViewById(R.id.tv_ads_content);
        tvAdsContent.setText(currBanner.getAdsContent());


        //event
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //chueyn man hinh(activity) explicit
                Intent intent = new Intent(context, SongListActivity.class);
                intent.putExtra("banner", currBanner);

                context.startActivity(intent);



                //Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();
            }
        });


        //display in viewpager or not
        container.addView(view);

        return view;
    }

    //xoa cac view di sau khi no duoc hien thi xong r
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
