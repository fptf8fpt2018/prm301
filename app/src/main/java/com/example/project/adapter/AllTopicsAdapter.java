package com.example.project.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.activity.GenreListByTopicActivity;
import com.example.project.models.Topic;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AllTopicsAdapter extends RecyclerView.Adapter<AllTopicsAdapter.ViewHolder> {

    private Context context;
    private List<Topic> topics;

    public AllTopicsAdapter(Context context, List<Topic> topics) {
        this.context = context;
        this.topics = topics;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.all_topics_each_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllTopicsAdapter.ViewHolder holder, int position) {
        Topic topic = topics.get(position);

        Picasso.with(context).load(topic.getTopicImagine()).into(holder.imgvTopicImage);
    }

    @Override
    public int getItemCount() {
        return topics.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imgvTopicImage;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imgvTopicImage = itemView.findViewById(R.id.imgv_all_topics_each_row_image);

            //event
            imgvTopicImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, GenreListByTopicActivity.class);
                    intent.putExtra("topic", topics.get(getPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
