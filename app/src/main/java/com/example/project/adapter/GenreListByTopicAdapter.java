package com.example.project.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.activity.SongListActivity;
import com.example.project.models.Genre;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GenreListByTopicAdapter extends RecyclerView.Adapter<GenreListByTopicAdapter.ViewHolder> {

    private Context context;
    private List<Genre> genres;

    public GenreListByTopicAdapter(Context context, List<Genre> genres) {
        this.context = context;
        this.genres = genres;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.genre_by_topic_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreListByTopicAdapter.ViewHolder holder, int position) {
        Genre genre = genres.get(position);

        Picasso.with(context).load(genre.getGenreImagine()).into(holder.imageViewGenre);
        holder.tvGenreName.setText(genre.getGenreName());
    }

    @Override
    public int getItemCount() {
        return genres.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageViewGenre;
        TextView tvGenreName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewGenre = itemView.findViewById(R.id.imgv_genre_by_topic_img);
            tvGenreName = itemView.findViewById(R.id.tv_genre_by_topic_name);

            //event
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SongListActivity.class);
                    intent.putExtra("genre", genres.get(getPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
