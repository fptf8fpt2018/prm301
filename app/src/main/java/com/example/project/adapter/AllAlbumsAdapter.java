package com.example.project.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.activity.SongListActivity;
import com.example.project.models.Album;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.zip.Inflater;


public class AllAlbumsAdapter extends RecyclerView.Adapter<AllAlbumsAdapter.ViewHolder> {

    private Context context;
    private List<Album> allAlbums;

    public AllAlbumsAdapter(Context context, List<Album> allAlbums) {
        this.context = context;
        this.allAlbums = allAlbums;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.all_albums_each_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllAlbumsAdapter.ViewHolder holder, int position) {
        Album album = allAlbums.get(position);

        holder.textViewAlbumName.setText(album.getAlbumName());
        Picasso.with(context).load(album.getAlbumImage()).into(holder.imageViewAlbumImg);
    }

    @Override
    public int getItemCount() {
        return allAlbums.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageViewAlbumImg;
        TextView textViewAlbumName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageViewAlbumImg =itemView.findViewById(R.id.imgv_all_albums_each_row_img);
            textViewAlbumName = itemView.findViewById(R.id.tv_all_albums_each_row_name);

            //event
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, SongListActivity.class);
                    intent.putExtra("album", allAlbums.get(getPosition()));
                    context.startActivity(intent);
                }
            });
        }
    }
}
