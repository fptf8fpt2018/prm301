package com.example.project.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SingSong implements Parcelable {
    @SerializedName("sing_song_id")
    @Expose
    private String singSongId;
    @SerializedName("sing_song_album_id")
    @Expose
    private String singSongAlbumId;
    @SerializedName("sing_song_genre_id")
    @Expose
    private String singSongGenreId;
    @SerializedName("sing_song_playlist_id")
    @Expose
    private String singSongPlaylistId;
    @SerializedName("sing_song_name")
    @Expose
    private String singSongName;
    @SerializedName("sing_song_image")
    @Expose
    private String singSongImage;
    @SerializedName("sing_song_singer")
    @Expose
    private String singSongSinger;
    @SerializedName("sing_song_link")
    @Expose
    private String singSongLink;
    @SerializedName("sing_song_total_like")
    @Expose
    private String singSongTotalLike;

    protected SingSong(Parcel in) {
        singSongId = in.readString();
        singSongAlbumId = in.readString();
        singSongGenreId = in.readString();
        singSongPlaylistId = in.readString();
        singSongName = in.readString();
        singSongImage = in.readString();
        singSongSinger = in.readString();
        singSongLink = in.readString();
        singSongTotalLike = in.readString();
    }

    public static final Creator<SingSong> CREATOR = new Creator<SingSong>() {
        @Override
        public SingSong createFromParcel(Parcel in) {
            return new SingSong(in);
        }

        @Override
        public SingSong[] newArray(int size) {
            return new SingSong[size];
        }
    };

    public String getSingSongId() {
        return singSongId;
    }

    public void setSingSongId(String singSongId) {
        this.singSongId = singSongId;
    }

    public String getSingSongAlbumId() {
        return singSongAlbumId;
    }

    public void setSingSongAlbumId(String singSongAlbumId) {
        this.singSongAlbumId = singSongAlbumId;
    }

    public String getSingSongGenreId() {
        return singSongGenreId;
    }

    public void setSingSongGenreId(String singSongGenreId) {
        this.singSongGenreId = singSongGenreId;
    }

    public String getSingSongPlaylistId() {
        return singSongPlaylistId;
    }

    public void setSingSongPlaylistId(String singSongPlaylistId) {
        this.singSongPlaylistId = singSongPlaylistId;
    }

    public String getSingSongName() {
        return singSongName;
    }

    public void setSingSongName(String singSongName) {
        this.singSongName = singSongName;
    }

    public String getSingSongImage() {
        return singSongImage;
    }

    public void setSingSongImage(String singSongImage) {
        this.singSongImage = singSongImage;
    }

    public String getSingSongSinger() {
        return singSongSinger;
    }

    public void setSingSongSinger(String singSongSinger) {
        this.singSongSinger = singSongSinger;
    }

    public String getSingSongLink() {
        return singSongLink;
    }

    public void setSingSongLink(String singSongLink) {
        this.singSongLink = singSongLink;
    }

    public String getSingSongTotalLike() {
        return singSongTotalLike;
    }

    public void setSingSongTotalLike(String singSongTotalLike) {
        this.singSongTotalLike = singSongTotalLike;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(singSongId);
        dest.writeString(singSongAlbumId);
        dest.writeString(singSongGenreId);
        dest.writeString(singSongPlaylistId);
        dest.writeString(singSongName);
        dest.writeString(singSongImage);
        dest.writeString(singSongSinger);
        dest.writeString(singSongLink);
        dest.writeString(singSongTotalLike);
    }
}
