package com.example.project.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Genre implements Serializable {

    @SerializedName("genre_id")
    @Expose
    private String genreId;
    @SerializedName("genre_name")
    @Expose
    private String genreName;
    @SerializedName("genre_imagine")
    @Expose
    private String genreImagine;
    @SerializedName("genre_topic_id")
    @Expose
    private String genreTopicId;

    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public String getGenreImagine() {
        return genreImagine;
    }

    public void setGenreImagine(String genreImagine) {
        this.genreImagine = genreImagine;
    }

    public String getGenreTopicId() {
        return genreTopicId;
    }

    public void setGenreTopicId(String genreTopicId) {
        this.genreTopicId = genreTopicId;
    }
}
