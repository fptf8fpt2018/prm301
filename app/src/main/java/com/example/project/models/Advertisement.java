package com.example.project.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

//https://jsonformatter.curiousconcept.com/ view json file easily
//https://www.jsonschema2pojo.org/ convert json to obj java
public class Advertisement implements Serializable {
        @SerializedName("ads_id")
        @Expose
        private String adsId;

        @SerializedName("ads_imagine")
        @Expose
        private String adsImagine;
        @SerializedName("ads_content")
        @Expose
        private String adsContent;
        @SerializedName("song_id")
        @Expose
        private String songId;
        @SerializedName("song_name")
        @Expose
        private String songName;
        @SerializedName("song_imagine")
        @Expose
        private String songImagine;

        public String getAdsId() {
            return adsId;
        }

        public void setAdsId(String adsId) {
            this.adsId = adsId;
        }

        public String getAdsImagine() {
            return adsImagine;
        }

        public void setAdsImagine(String adsImagine) {
            this.adsImagine = adsImagine;
        }

        public String getAdsContent() {
            return adsContent;
        }

        public void setAdsContent(String adsContent) {
            this.adsContent = adsContent;
        }

        public String getSongId() {
            return songId;
        }

        public void setSongId(String songId) {
            this.songId = songId;
        }

        public String getSongName() {
            return songName;
        }

        public void setSongName(String songName) {
            this.songName = songName;
        }

        public String getSongImagine() {
            return songImagine;
        }

        public void setSongImagine(String songImagine) {
            this.songImagine = songImagine;
        }

}
