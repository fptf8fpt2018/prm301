package com.example.project.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TopicMergeGenre {
    //JSON name mapper
    @SerializedName("Genres")
    @Expose
    private List<Genre> genres = null;

    //JSON name mapper
    @SerializedName("Topics")
    @Expose
    private List<Topic> topics = null;



    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }
}
