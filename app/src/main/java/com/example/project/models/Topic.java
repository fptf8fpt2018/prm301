package com.example.project.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Topic implements Serializable {

    @SerializedName("topic_id")
    @Expose
    private String topicId;
    @SerializedName("topic_name")
    @Expose
    private String topicName;
    @SerializedName("topic_imagine")
    @Expose
    private String topicImagine;

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getTopicImagine() {
        return topicImagine;
    }

    public void setTopicImagine(String topicImagine) {
        this.topicImagine = topicImagine;
    }

}