package com.example.project.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.sql.Time;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//se tra ve du lieu da cau hinh theo retrofit
public class APIRetrofitClient {

    //from thu vien ben ngoai
    private static Retrofit retrofit = null;

    //tra ve cau hinh
    public static Retrofit getClient(String baseUrl){
        //giao thuc mang, tuong tac mang phia server
        OkHttpClient okHttpClient
                = new OkHttpClient.Builder()
                .readTimeout(1000000 , TimeUnit.MICROSECONDS)
                .writeTimeout(1000000 , TimeUnit.MICROSECONDS)
                .connectTimeout(1000000 ,TimeUnit.MICROSECONDS)
                .retryOnConnectionFailure(true)//loi ve mang se co gang ket noi lai
                .protocols(Arrays.asList(Protocol.HTTP_1_1))//giao thuc mang
                .build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))//conver json ve du lieu java
                .build();

        return retrofit;

    }
}
