package com.example.project.service;


import com.example.project.models.Advertisement;
import com.example.project.models.Album;
import com.example.project.models.Genre;
import com.example.project.models.Playlist;
import com.example.project.models.SingSong;
import com.example.project.models.Topic;
import com.example.project.models.TopicMergeGenre;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

//gui len phuong thuc de tuong tac server, va tra ve du lieu cho thang nay
public interface DataService {

    //server tra ve
    @GET("songBanner.php")
    Call<List<Advertisement>> selectSongBanner();

    @GET("threeNewestPlaylist.php")
    Call<List<Playlist>> select3newestPlaylist();

    @GET("eightNewestTopicMergeGenre.php")
    Call<TopicMergeGenre> select8newestTopicAndGenre();

    @GET("fourNewestAlbums.php")
    Call<List<Album>> select4newestAlbums();

    @GET("fiveFavoriteSong.php")
    Call<List<SingSong>> select5hotSong();

    @FormUrlEncoded
    @POST("songListFromForeignKey.php")
    Call<List<SingSong>> selectSongsFromAds(@Field("ads_id") String adsId);

    @FormUrlEncoded
    @POST("songListFromForeignKey.php")
    Call<List<SingSong>> selectSongsFromPlaylist(@Field("playlist_id") String playlistId);

    @GET("allPlaylists.php")
    Call<List<Playlist>> selectAllPlaylists();

    @FormUrlEncoded
    @POST("songListFromForeignKey.php")
    Call<List<SingSong>> selectSongsFromGenre(@Field("genre_id") String genreId);

    @GET("allTopics.php")
    Call<List<Topic>> selectAllTopics();

    @FormUrlEncoded
    @POST("genresByTopic.php")
    Call<List<Genre>> selectGenresByTopic(@Field("topic_id") String topicId);

    @GET("allAlbums.php")
    Call<List<Album>> selectAllAlbums();

    @FormUrlEncoded
    @POST("songListFromForeignKey.php")
    Call<List<SingSong>> selectSongsFromAlbum(@Field("album_id") String albumId);

    @FormUrlEncoded
    @POST("updateLikeOfSong.php")
    Call<String> updateLike(@Field("song_id") String songId,
                            @Field("total_like_per_times") String totalLikePerTimes);

    @FormUrlEncoded
    @POST("searchSongsByKeyword.php")
    Call<List<SingSong>> selectSongsByIncluedNamm(@Field("keyword") String keyword);

}
