package com.example.project.service;

//trung gian giua interface DataService va class APIRetrofitClient
public class APIService {

    private static String baselUrl = "https://trieuddhp230599.000webhostapp.com/android_mp3/server/";

    //du lieu tra ve cho dataservice
    public static DataService getService(){
        //khoi tao nhung function trong dataservice
        return APIRetrofitClient.getClient(baselUrl).create(DataService.class);
    }
}
