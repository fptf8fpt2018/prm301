package com.example.project.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.example.project.R;
import com.example.project.adapter.AlbumAdapter;
import com.example.project.adapter.AllAlbumsAdapter;
import com.example.project.models.Album;
import com.example.project.service.APIService;
import com.example.project.service.DataService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllAlbumsActivity extends AppCompatActivity {

    Toolbar toolbarAllAlbums;
    RecyclerView recyclerViewAllAlbums;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_albums);

        mapView();

        initToolbar();

        setDataFromAPI();
    }



    private void mapView() {
        toolbarAllAlbums = findViewById(R.id.toolbar_all_albums);
        recyclerViewAllAlbums = findViewById(R.id.recyler_view_all_albums_in_see_more);
    }

    private void initToolbar() {
        setSupportActionBar(toolbarAllAlbums);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("All Albums");
        toolbarAllAlbums.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setDataFromAPI() {
        DataService dataService = APIService.getService();
        Call<List<Album>> callback = dataService.selectAllAlbums();

        callback.enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                List<Album> allAlbums =  response.body();
                allAlbums.addAll(allAlbums);
                allAlbums.addAll(allAlbums);
                allAlbums.addAll(allAlbums);
                allAlbums.addAll(allAlbums);

                GridLayoutManager gridLayoutManager
                        = new GridLayoutManager(AllAlbumsActivity.this, 2);
                recyclerViewAllAlbums.setLayoutManager(gridLayoutManager);

                AllAlbumsAdapter allAlbumsAdapter
                        = new AllAlbumsAdapter(AllAlbumsActivity.this, allAlbums);
                recyclerViewAllAlbums.setAdapter(allAlbumsAdapter);

                //Log.d("bbb", allAlbums.get(0).getAlbumName());
            }

            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {
                Log.d("bbb", "Failed at AllAlbumsActivity.java/ func setDataFromAPI()");
            }
        });
    }


}