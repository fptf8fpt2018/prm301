package com.example.project.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.adapter.ViewPagerMusicPlayerAdapter;
import com.example.project.fragment.FragmentMusicDisc;
import com.example.project.fragment.FragmentMusicPlayerSongList;
import com.example.project.models.SingSong;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MusicPlayerActivity extends AppCompatActivity {

    Toolbar toolbarMusicPlayer;
    SeekBar seekBarMusicPlayer;
    ViewPager viewPagerMusicPlayer;
    ViewPagerMusicPlayerAdapter viewPagerMusicPlayerAdapter;
    FragmentMusicPlayerSongList fragmentMusicPlayerSongList;
    FragmentMusicDisc fragmentMusicDisc;
    TextView tvCurTime, tvTotalTime;
    ImageButton imgPlay, imgRepeat, imgNext, imgPreview, imgShuffee;

    public static final List<SingSong> finalSongs = new ArrayList<>();// truyen sang list nhac ben trai

    MediaPlayer mediaPlayer;
    int curSongIndex = 0;//next, previous
    boolean isRepeat = false;
    boolean isShuffle = false;
    boolean isNext = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_player);
        //dung de kiem tra tin hieu mang
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mapView();//mapping obj code vs UI

        initHardcodeViewPager();//init view paper hard-code, no need data

        getDataIntent();//get data from list songs(playlist, album, genre) or single song(ads, hot)

        initDynamicToolbar();//use data from intent






        //neu co it nhat 1 bai hat thi play bai dau tien va doi giao dien(title toolbar, nut play) phu hop
        if(finalSongs != null && finalSongs.size() > 0){
            SingSong firstSong = finalSongs.get(0);
            new MusicPlayer().execute(firstSong.getSingSongLink());

            getSupportActionBar().setTitle(firstSong.getSingSongName() + " - " + firstSong.getSingSongSinger());
            imgPlay.setImageResource(R.drawable.iconpause);
            //lay lai hard-code obj music disc de set data phia duoi
            fragmentMusicDisc
                    = (FragmentMusicDisc) viewPagerMusicPlayerAdapter.getItem(1); //dia nhac o thu 2 trong adapter
        }

        initDynamicMusicDisc();

        setEventClickPlayPause();






        setEventClickRepeat();
        setEventClickShuffle();
        setEventChangeSeekBarTime();
        setEventClickNext();
        setEventClickPrevious();

    }



    private void mapView() {
        toolbarMusicPlayer = findViewById(R.id.toolbar_music_player);
        seekBarMusicPlayer = findViewById(R.id.seek_bar_music_player);
        viewPagerMusicPlayer = findViewById(R.id.view_pager_music_player);
        tvCurTime = findViewById(R.id.tv_music_player_time);
        tvTotalTime = findViewById(R.id.tv_music_player_total_time);
        imgPlay = findViewById(R.id.img_btn_play);
        imgRepeat = findViewById(R.id.img_btn_repeat);
        imgNext = findViewById(R.id.img_btn_next);
        imgPreview = findViewById(R.id.img_btn_preview);
        imgShuffee = findViewById(R.id.img_btn_shuffee);
    }

    private void initHardcodeViewPager() {
        viewPagerMusicPlayerAdapter = new ViewPagerMusicPlayerAdapter(getSupportFragmentManager());
        fragmentMusicPlayerSongList = new FragmentMusicPlayerSongList();
        fragmentMusicDisc = new FragmentMusicDisc();

        viewPagerMusicPlayerAdapter.addFragment(fragmentMusicPlayerSongList);
        viewPagerMusicPlayerAdapter.addFragment(fragmentMusicDisc);

        viewPagerMusicPlayer.setAdapter(viewPagerMusicPlayerAdapter);
        viewPagerMusicPlayer.setCurrentItem(1);// display dia nhac truoc, quet sang trai la song list
    }

    private void getDataIntent() {
        //delete old data before
        finalSongs.clear();

        Intent intent = getIntent();
        if(intent != null){

            //lay duoc data 1 bai
            if(intent.hasExtra("song")){
                SingSong song = intent.getParcelableExtra("song");
                finalSongs.add(song);
                //Log.d("bbb", song.getSingSongName());
                //Toast.makeText(this, song.getSingSongName(), Toast.LENGTH_LONG).show();
            }

            //lay duoc 1 list bai hat tu playlist, album, genre
            else if(intent.hasExtra("songs")){
                List<SingSong> songs = intent.getParcelableArrayListExtra("songs");

                finalSongs.addAll(songs);
           /* for(SingSong song : songs){
                Log.d("bbb", song.getSingSongName());
            }*/
            }
        }
    }

    private void initDynamicToolbar() {
        setSupportActionBar(toolbarMusicPlayer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(finalSongs.get(0).getSingSongName() + " - " + finalSongs.get(0).getSingSongSinger());


        toolbarMusicPlayer.setTitleTextColor(Color.WHITE);
        //click tro lai
        toolbarMusicPlayer.setNavigationOnClickListener(v -> {
            finish(); //end activity to backstack
            mediaPlayer.stop();//
            finalSongs.clear();//xoa old state
        });
    }

    /*-------------------------Dynamic ViewPager----------------*/
    //thuc hien 1 ca khuc
    class MusicPlayer extends AsyncTask<String, Void, String>{

        //Worker thread
        //tham so thu nhat(param) dau vao, thu3(params) la dau ra trong generic
        @Override
        protected String doInBackground(String... strings) {
            return strings[0];
        }

        //Update UI
        //tham so thu 3(param) dau vao
        @Override
        protected void onPostExecute(String songLink) {
            super.onPostExecute(songLink);
            try {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC); //lay nhac online
                //thang nay chay theo kieu bat dong bo --> thoi gian nhay vao 1 so nao do se bi loi
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mediaPlayer.stop();
                        mediaPlayer.reset();
                    }
                });
                //den day khoi tao an toan roi


                mediaPlayer.setDataSource(songLink); //catch la checked exception cua thang nay
                mediaPlayer.prepare();//media muon phat duoc phai goi ham nay
            } catch (IOException e) {
                e.printStackTrace();
            }

            mediaPlayer.start();
            displaySongTimeState();
            updateSongTimeState();//counter time inscrease change
        }
    }

    private void displaySongTimeState() {
        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
        int totalTimeInSec = mediaPlayer.getDuration();
        tvTotalTime.setText(sdf.format(totalTimeInSec));

        seekBarMusicPlayer.setMax(totalTimeInSec);
    }


    private void initDynamicMusicDisc() {
        //Runnable xu ly cong viec nang nhoc
        //Handler cap nhat UI
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //fragment dia nhac co get dc du lieu bai hat ra hay ko
                if(viewPagerMusicPlayerAdapter.getItem(1 ) != null){
                    //du lieu bai hat co hay khong
                    if(finalSongs != null && finalSongs.size() > 0){
                        SingSong firstSong = finalSongs.get(0);
                        fragmentMusicDisc.setImg(firstSong.getSingSongImage());
                        //xoa di cai thang ngoi ngong update UI nay sau khi hinh bai hat duoc load len thanh cong
                        handler.removeCallbacks(this);
                    }else{
                        //chay lai chinh no neu chua lay duoc anh ra, moi lan chay cach nhau 0.3s
                        handler.postDelayed(this, 300);
                    }
                }
            }
        }, 500);
    }


    private void setEventClickPlayPause() {

        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //trinh phat nhac dang chay
                if(mediaPlayer.isPlaying()){
                    mediaPlayer.pause();
                    imgPlay.setImageResource(R.drawable.iconplay);
                }
                //dang dung thi nghe
                else{
                    mediaPlayer.start();
                    imgPlay.setImageResource(R.drawable.iconpause);
                }
            }
        });
    }



    private void setEventClickRepeat() {
        imgRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //repeat(true)
                if(isRepeat == false){
                    //repeat(true) ^ shuffle(false)
                    if(isShuffle == true){
                        isShuffle = false;
                        imgShuffee.setImageResource(R.drawable.iconsuffle);
                    }
                    imgRepeat.setImageResource(R.drawable.iconsyned);
                    isRepeat = true;
                }
                //repeat(false)
                else{
                    imgRepeat.setImageResource(R.drawable.iconrepeat);
                    isRepeat = false;
                }
            }
        });

        //sau 0.5s moi cho bam tiep, tranh nhieu luong tao ra gay crash
        imgRepeat.setClickable(false);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                imgRepeat.setClickable(true);
            }
        },500);
    }


    private void setEventClickShuffle() {
        imgShuffee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //shuffle(true)
                if(isShuffle == false){
                    //shuffle(true) ^ repeat(false)
                    if(isRepeat == true){
                        isRepeat = false;
                        imgRepeat.setImageResource(R.drawable.iconrepeat);
                    }
                    imgShuffee.setImageResource(R.drawable.iconshuffled);
                    isShuffle = true;
                }
                //shuffle(false)
                else{
                    imgShuffee.setImageResource(R.drawable.iconsuffle);
                    isShuffle = false;
                }
            }
        });

        //sau 0.5s moi cho bam tiep, tranh nhieu luong tao ra gay crash
        imgShuffee.setClickable(false);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                imgShuffee.setClickable(true);
            }
        },500);
    }


    private void setEventChangeSeekBarTime() {
        seekBarMusicPlayer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            //nguoi dung keo roi tha ra
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.seekTo(seekBar.getProgress());
            }
        });
    }

    private void setEventClickNext()  {
        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //co du lieu
                if(finalSongs != null && finalSongs.size() > 0){
                    //co dang duoc khoi tao hay dang phat nhac khong
                    if(mediaPlayer != null || mediaPlayer.isPlaying()){
                        mediaPlayer.stop();
                        mediaPlayer.release();//dong bo lai
                        mediaPlayer = null;
                    }

                    //out of index
                    if(curSongIndex < finalSongs.size()){
                        imgPlay.setImageResource(R.drawable.iconpause);
                        curSongIndex ++;


                        //on shuffle mode
                        if(isShuffle == true){
                            Random random = new Random();
                            int index;
                            //get random different from current index
                            do{
                                index = random.nextInt(finalSongs.size());
                            }while (index == curSongIndex);
                            curSongIndex = index;
                        }

                        //on repeat mode
                        else if(isRepeat  == true){
                            //cho = size de xuong duoi se check out of bound cho ve 0
                            if(curSongIndex == 0){
                                curSongIndex = finalSongs.size();
                            }
                            //tru di thang vua cong ben tren
                            curSongIndex -= 1;
                        }

                        //out of index --> reset to 0
                        if (curSongIndex > finalSongs.size() - 1){
                            curSongIndex = 0;
                        }


                        //update GUI, musicPlayer by song
                        SingSong curSong = finalSongs.get(curSongIndex);
                        new MusicPlayer().execute(curSong.getSingSongLink());
                        fragmentMusicDisc.setImg(curSong.getSingSongImage());
                        getSupportActionBar().setTitle(curSong.getSingSongName() + " - " + curSong.getSingSongSinger());


                        updateSongTimeState();
                    }
                }

                //sau 5s moi cho bam tiep, tranh nhieu luong tao ra gay crash
                imgNext.setClickable(false);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imgNext.setClickable(true);
                    }
                },5000);
            }
        });
    }

    private void setEventClickPrevious() {
        imgPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //co du lieu
                if(finalSongs != null && finalSongs.size() > 0){
                    //co dang duoc khoi tao hay dang phat nhac khong
                    if(mediaPlayer != null || mediaPlayer.isPlaying()){
                        mediaPlayer.stop();
                        mediaPlayer.release();//dong bo lai
                        mediaPlayer = null;
                    }

                    //out of index
                    if(curSongIndex < finalSongs.size()){
                        imgPlay.setImageResource(R.drawable.iconpause);
                        curSongIndex --;

                        //out of index --> reset to last index
                        if(curSongIndex < 0){
                            curSongIndex = finalSongs.size() - 1;
                        }


                        //on shuffle mode
                        if(isShuffle == true){
                            Random random = new Random();
                            int index;
                            //get random different from current index
                            do{
                                index = random.nextInt(finalSongs.size());
                            }while (index == curSongIndex);
                            curSongIndex = index;
                        }

                        //on repeat mode
                        else if(isRepeat  == true){
                            //cong them 1 thang o ben tren
                            curSongIndex += 1;
                        }


                        //update GUI, musicPlayer by song
                        SingSong curSong = finalSongs.get(curSongIndex);
                        new MusicPlayer().execute(curSong.getSingSongLink());
                        fragmentMusicDisc.setImg(curSong.getSingSongImage());
                        getSupportActionBar().setTitle(curSong.getSingSongName() + " - " + curSong.getSingSongSinger());



                        updateSongTimeState();
                    }
                }

                //sau 5s moi cho bam tiep, tranh nhieu luong tao ra gay crash
                imgPreview.setClickable(false);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imgPreview.setClickable(true);
                    }
                },5000);
            }
        });
    }



    private void updateSongTimeState(){
        //update lien tuc sau 0.3s
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //phai co media moi update lai time
                if(mediaPlayer != null){
                    //media chay den dau thi cap nhat lai seekbar nhu vay
                    seekBarMusicPlayer.setProgress(mediaPlayer.getCurrentPosition());
                    SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
                    int curTimeInSec = mediaPlayer.getCurrentPosition();
                    tvCurTime.setText(sdf.format(curTimeInSec));
                    handler.postDelayed(this, 300);

                    //chay den khoang thoi gian cuoi cung
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            isNext = true;
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        },300);


        Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isNext == true){
                    //out of index
                    if(curSongIndex < finalSongs.size()){
                        imgPlay.setImageResource(R.drawable.iconpause);
                        curSongIndex ++;


                        //on shuffle mode
                        if(isShuffle == true){
                            Random random = new Random();
                            int index;
                            //get random different from current index
                            do{
                                index = random.nextInt(finalSongs.size());
                            }while (index == curSongIndex);
                            curSongIndex = index;
                        }

                        //on repeat mode
                        else if(isRepeat  == true){
                            //cho = size de xuong duoi se check out of bound cho ve 0
                            if(curSongIndex == 0){
                                curSongIndex = finalSongs.size();
                            }
                            //tru di thang vua cong ben tren
                            curSongIndex -= 1;
                        }

                        //out of index --> reset to 0
                        if (curSongIndex > finalSongs.size() - 1){
                            curSongIndex = 0;
                        }


                        //update GUI, musicPlayer by song
                        SingSong curSong = finalSongs.get(curSongIndex);
                        new MusicPlayer().execute(curSong.getSingSongLink());
                        fragmentMusicDisc.setImg(curSong.getSingSongImage());
                        getSupportActionBar().setTitle(curSong.getSingSongName() + " - " + curSong.getSingSongSinger());

                    }


                    //sau 5s moi cho bam tiep, tranh nhieu luong tao ra gay crash
                    imgNext.setClickable(false);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            imgNext.setClickable(true);
                        }
                    },5000);


                    isNext = false;
                }else{
                    handler1.postDelayed(this, 1000);
                }
            }
        }, 1000);
    }
}