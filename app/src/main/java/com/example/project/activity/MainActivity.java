package com.example.project.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import com.example.project.R;
import com.example.project.adapter.ViewPagerMainAdapter;
import com.example.project.fragment.FragmentHome;
import com.example.project.fragment.FragmentSearch;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestAppPermission(Manifest.permission.INTERNET, 1000);
        map();
        init();
    }

    public void requestAppPermission(String permission, int code){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{permission}, code);
            }
        }
    }

    private void map() {
        tabLayout = findViewById(R.id.myTabLayout);
        viewPager = findViewById(R.id.myViewPager);
    }



    private void init(){
        ViewPagerMainAdapter mainViewPagerAdapter
                = new ViewPagerMainAdapter(getSupportFragmentManager());

        mainViewPagerAdapter.addFragment(new FragmentHome(),"Home");
        mainViewPagerAdapter.addFragment(new FragmentSearch(),"Search");

        viewPager.setAdapter(mainViewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.icontrangchu);
        tabLayout.getTabAt(1).setIcon(R.drawable.icontimkiem);
    }
}