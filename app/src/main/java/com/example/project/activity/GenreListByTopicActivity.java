package com.example.project.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.adapter.GenreListByTopicAdapter;
import com.example.project.models.Genre;
import com.example.project.models.Topic;
import com.example.project.service.APIService;
import com.example.project.service.DataService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenreListByTopicActivity extends AppCompatActivity {

    Toolbar toolbarGenresByTopic;
    RecyclerView recyclerViewGenresByTopic;


    Topic topic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre_list_by_topic);

        mapView();

        getIntentData();

        initToolbar();//need data from intent

        setDataFromAPI();
    }



    private void mapView() {
         recyclerViewGenresByTopic = findViewById(R.id.recyler_view_genres_by_topic);
         toolbarGenresByTopic = findViewById(R.id.toolbar_genres_by_topic);
    }


    private void initToolbar() {
        setSupportActionBar(toolbarGenresByTopic);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(topic.getTopicName());
        toolbarGenresByTopic.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private void getIntentData() {
        Intent intent = getIntent();
        if(intent.hasExtra("topic")){
            topic = (Topic) intent.getSerializableExtra("topic");

            //Toast.makeText(this, topic.getTopicName(), Toast.LENGTH_LONG).show();
        }
    }

    private void setDataFromAPI() {
        DataService dataService = APIService.getService();
        Call<List<Genre>> callback = dataService.selectGenresByTopic(topic.getTopicId());

        callback.enqueue(new Callback<List<Genre>>() {
            @Override
            public void onResponse(Call<List<Genre>> call, Response<List<Genre>> response) {
                List<Genre> genres = response.body();
                genres.addAll(genres);
                genres.addAll(genres);
                genres.addAll(genres);

                GridLayoutManager gridLayoutManager
                        = new GridLayoutManager(GenreListByTopicActivity.this, 2);
                recyclerViewGenresByTopic.setLayoutManager(gridLayoutManager);

                GenreListByTopicAdapter genreListByTopicAdapter
                        = new GenreListByTopicAdapter(GenreListByTopicActivity.this, genres);
                recyclerViewGenresByTopic.setAdapter(genreListByTopicAdapter);


                //Log.d("bbb",genres.get(0).getGenreName());
            }

            @Override
            public void onFailure(Call<List<Genre>> call, Throwable t) {
                Log.d("bbb", "Failed at GenreListByTopicActivity.java/ func getData()");
            }
        });
    }


}