package com.example.project.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.adapter.SongListAdapter;
import com.example.project.models.Advertisement;
import com.example.project.models.Album;
import com.example.project.models.Genre;
import com.example.project.models.Playlist;
import com.example.project.models.SingSong;
import com.example.project.service.APIService;
import com.example.project.service.DataService;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SongListActivity extends AppCompatActivity {

    CoordinatorLayout coordinatorLayout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Toolbar toolbarSongList;
    RecyclerView recyclerViewSongList;
    FloatingActionButton floatingActionButton;
    ImageView imgvSongList;

    //pull data from obj from other activity, fragment
    Advertisement ads;
    Playlist playlist;
    Genre genre;
    Album album;
    //push data into obj in current activity
    List<SingSong> songs;    //include will push data to MusicPlayerActivity


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_list);
        //dung de kiem tra tin hieu mang
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mapVieW();

        getDataIntent();

        initToolbar();//set data to toolbar in setDataIntent

        setDataIntent();

        setEvent();//sau khi co du lieu va giao dien duoc hien thi
    }




    private void mapVieW() {
        floatingActionButton = findViewById(R.id.floating_action_btn);
        //ko cho click khi du lieu chua do het vao
        enableDisableViewBehaviour(floatingActionButton, null, false);

        coordinatorLayout = findViewById(R.id.coordinator_layout);
        collapsingToolbarLayout = findViewById(R.id.collaping_toolbar_layout);
        toolbarSongList = findViewById(R.id.toolbar_song_list);
        recyclerViewSongList = findViewById(R.id.recyler_view_song_list);
        imgvSongList = findViewById(R.id.imgv_song_list);
    }

    //ref: https://stackoverflow.com/questions/35052100/how-to-enable-disable-floatingactionbutton-behavior
    public static void enableDisableViewBehaviour(View view,CoordinatorLayout.Behavior<View> behavior,boolean enable){
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
        params.setBehavior(behavior);
        view.requestLayout();
        view.setVisibility((enable ? View.VISIBLE: View.GONE));
    }

    private void getDataIntent() {
        Intent intent = getIntent();

        if(intent != null){
            if(intent.hasExtra("banner")){
                 ads = (Advertisement) intent.getSerializableExtra("banner");

                //Toast.makeText(this, ads.getSongName(), Toast.LENGTH_LONG).show();
            }

            else if(intent.hasExtra("playlist")){
                playlist = (Playlist) intent.getSerializableExtra("playlist");

                //Toast.makeText(this, playlist.getPlaylistName(), Toast.LENGTH_LONG).show();
            }

            else if(intent.hasExtra("genre")){
                genre = (Genre) intent.getSerializableExtra("genre");

                //Toast.makeText(this, genre.getGenreName(), Toast.LENGTH_LONG).show();
            }

            else if(intent.hasExtra("album")){
                album = (Album) intent.getSerializableExtra("album");

                //Toast.makeText(this, album.getAlbumName(), Toast.LENGTH_LONG).show();
            }
        }
    }


    private void initToolbar() {
        //project name show
        setSupportActionBar(toolbarSongList);
        //mui ten tro ve
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarSongList.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //phong to nhat
        collapsingToolbarLayout.setExpandedTitleColor(Color.WHITE);
        //thu nho nhat
        collapsingToolbarLayout.setCollapsedTitleTextColor(Color.WHITE);
    }


    private void setValueInToolbar(String songName, String songImg) {
        Picasso.with(this).load(songImg).into(imgvSongList);

        collapsingToolbarLayout.setTitle(songName);
        try {
            URL url = new URL(songImg);
            Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);

            //> API16
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
                collapsingToolbarLayout.setBackground(bitmapDrawable);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private void setDataIntent() {
        //valid data from ads
        if(ads != null && !ads.getSongName().equals("")){
            setValueInToolbar(ads.getSongName(), ads.getSongImagine());
            getSongFromAds(ads.getAdsId());
        }

        //valid data from playlist
        else if(playlist != null && !playlist.getPlaylistName().equals("")){
            setValueInToolbar(playlist.getPlaylistName(), playlist.getPlaylistImagine());
            getSongFromPlaylist(playlist.getPlaylistId());
        }

        //valid data from genre
        else if(genre != null && !genre.getGenreName().equals("")){
            setValueInToolbar(genre.getGenreName(), genre.getGenreImagine());
            getSongFromGenre(genre.getGenreId());
        }

        //valid data from album
        else if(album != null && !album.getAlbumName().equals("")){
            setValueInToolbar(album.getAlbumName(), album.getAlbumImage());
            getSongFromAlbum(album.getAlbumId());
        }
    }


    private void getSongFromAds(String adsId) {
        DataService dataService = APIService.getService();
        Call<List<SingSong>> callback = dataService.selectSongsFromAds(adsId);
        callback.enqueue(new Callback<List<SingSong>>() {
            @Override
            public void onResponse(Call<List<SingSong>> call, Response<List<SingSong>> response) {
                songs = response.body();
                songs.addAll(songs);


                LinearLayoutManager linearLayoutManager
                        = new LinearLayoutManager(SongListActivity.this);
                //linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
                recyclerViewSongList.setLayoutManager(linearLayoutManager);

                SongListAdapter songListAdapter
                        = new SongListAdapter(SongListActivity.this, songs);
                recyclerViewSongList.setAdapter(songListAdapter);

                //sau khi co het du lieu tren man hinh moi cho hien thi nut va sau do set event cho no
                enableDisableViewBehaviour(floatingActionButton, new BottomSheetBehavior<>(), true);

                //Log.d("bbb", songs.get(0).getSingSongName());
            }

            @Override
            public void onFailure(Call<List<SingSong>> call, Throwable t) {
                Log.d("bbb", "Failed at SongListActivity.java/ func getSongFromAds(adsId)");
            }
        });
    }

    private void getSongFromPlaylist(String playlistId) {
        DataService dataService = APIService.getService();
        Call<List<SingSong>> callback = dataService.selectSongsFromPlaylist(playlistId);
        callback.enqueue(new Callback<List<SingSong>>() {
            @Override
            public void onResponse(Call<List<SingSong>> call, Response<List<SingSong>> response) {
                songs =  response.body();


                LinearLayoutManager linearLayoutManager
                        = new LinearLayoutManager(SongListActivity.this);
                recyclerViewSongList.setLayoutManager(linearLayoutManager);


                SongListAdapter songListAdapter
                        = new SongListAdapter(SongListActivity.this, songs);
                recyclerViewSongList.setAdapter(songListAdapter);


                //sau khi co het du lieu tren man hinh moi cho hien thi nut va sau do set event cho no
                enableDisableViewBehaviour(floatingActionButton, new BottomSheetBehavior<>(), true);

                //Log.d("bbb", songs.get(0).getSingSongId());
            }

            @Override
            public void onFailure(Call<List<SingSong>> call, Throwable t) {
                Log.d("bbb", "Failed at SongListActivity.java/ func getSongFromPlaylist(adsId)");
            }
        });

    }

    private void getSongFromGenre(String genreId) {
        DataService dataService = APIService.getService();
        Call<List<SingSong>> callback = dataService.selectSongsFromGenre(genreId);
        callback.enqueue(new Callback<List<SingSong>>() {
            @Override
            public void onResponse(Call<List<SingSong>> call, Response<List<SingSong>> response) {
                songs =  response.body();


                LinearLayoutManager linearLayoutManager
                        = new LinearLayoutManager(SongListActivity.this);
                recyclerViewSongList.setLayoutManager(linearLayoutManager);


                SongListAdapter songListAdapter
                        = new SongListAdapter(SongListActivity.this, songs);
                recyclerViewSongList.setAdapter(songListAdapter);


                //sau khi co het du lieu tren man hinh moi cho hien thi nut va sau do set event cho no
                enableDisableViewBehaviour(floatingActionButton, new BottomSheetBehavior<>(), true);

                //Log.d("bbb", songs.get(0).getSingSongId());
            }

            @Override
            public void onFailure(Call<List<SingSong>> call, Throwable t) {
                Log.d("bbb", "Failed at SongListActivity.java/ func getSongFromGenre(adsId)");
            }
        });

    }

    private void getSongFromAlbum(String albumId) {
        DataService dataService = APIService.getService();
        Call<List<SingSong>> callback = dataService.selectSongsFromAlbum(albumId);
        callback.enqueue(new Callback<List<SingSong>>() {
            @Override
            public void onResponse(Call<List<SingSong>> call, Response<List<SingSong>> response) {
                songs =  response.body();


                LinearLayoutManager linearLayoutManager
                        = new LinearLayoutManager(SongListActivity.this);
                recyclerViewSongList.setLayoutManager(linearLayoutManager);


                SongListAdapter songListAdapter
                        = new SongListAdapter(SongListActivity.this, songs);
                recyclerViewSongList.setAdapter(songListAdapter);


                //sau khi co het du lieu tren man hinh moi cho hien thi nut va sau do set event cho no
                enableDisableViewBehaviour(floatingActionButton, new BottomSheetBehavior<>(), true);

                //Log.d("bbb", songs.get(0).getSingSongId());
            }

            @Override
            public void onFailure(Call<List<SingSong>> call, Throwable t) {
                Log.d("bbb", "Failed at SongListActivity.java/ func getSongFromAlbum(albumId)");
            }
        });

    }

    private void setEvent(){
        //du lieu da co
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SongListActivity.this, MusicPlayerActivity.class);
                intent.putParcelableArrayListExtra("songs", (ArrayList<? extends Parcelable>) songs);
                startActivity(intent);
            }
        });
    }

}