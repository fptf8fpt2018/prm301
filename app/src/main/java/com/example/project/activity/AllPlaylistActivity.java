package com.example.project.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.project.R;
import com.example.project.adapter.AllPlaylistsAdapter;
import com.example.project.models.Playlist;
import com.example.project.service.APIService;
import com.example.project.service.DataService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllPlaylistActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_playlist);

        mapView();

        init();

        getViewData();
    }


    private void mapView() {
        toolbar = findViewById(R.id.toolbar_all_playlist);
        recyclerView = findViewById(R.id.recyler_view_all_playlist);
    }

    private void init() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("All Playlists");
        toolbar.setTitleTextColor(getResources().getColor(R.color.purplrByTrieudd));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void getViewData() {
        DataService dataService = APIService.getService();
        Call<List<Playlist>> callback = dataService.selectAllPlaylists();

        callback.enqueue(new Callback<List<Playlist>>() {
            @Override
            public void onResponse(Call<List<Playlist>> call, Response<List<Playlist>> response) {
                List<Playlist> allPlaylists = response.body();
                allPlaylists.addAll(allPlaylists);
                allPlaylists.addAll(allPlaylists);
                allPlaylists.addAll(allPlaylists);


                AllPlaylistsAdapter allPlaylistsAdapter
                        = new AllPlaylistsAdapter(AllPlaylistActivity.this, allPlaylists);
                recyclerView.setAdapter(allPlaylistsAdapter);

                GridLayoutManager linearLayoutManager
                        = new GridLayoutManager(AllPlaylistActivity.this, 2);
                recyclerView.setLayoutManager(linearLayoutManager);

                //Log.d("bbb", allPlaylists.get(0).getPlaylistName());
            }

            @Override
            public void onFailure(Call<List<Playlist>> call, Throwable t) {
                Log.d("bbb", "Failed at AllPlaylistActivity.java");
            }
        });
    }
}