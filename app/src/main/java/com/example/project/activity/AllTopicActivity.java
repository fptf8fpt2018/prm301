package com.example.project.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.adapter.AllTopicsAdapter;
import com.example.project.models.Topic;
import com.example.project.service.APIService;
import com.example.project.service.DataService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllTopicActivity extends AppCompatActivity {

    RecyclerView recyclerViewAllTopics;
    Toolbar toolbarAllTopics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_topic);

        mapView();

        initToolbar();

        setDataActivityFromAPI();
    }



    private void mapView() {
        recyclerViewAllTopics = findViewById(R.id.recyler_view_all_topics);
        toolbarAllTopics = findViewById(R.id.toolbar_all_topics);
    }

    private void initToolbar() {
        setSupportActionBar(toolbarAllTopics);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("All Topics");
        toolbarAllTopics.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private void setDataActivityFromAPI() {
        DataService dataService = APIService.getService();
        Call<List<Topic>> callback = dataService.selectAllTopics();
        callback.enqueue(new Callback<List<Topic>>() {
            @Override
            public void onResponse(Call<List<Topic>> call, Response<List<Topic>> response) {
                List<Topic> allTopics = response.body();
                allTopics.addAll(allTopics);
                allTopics.addAll(allTopics);
                allTopics.addAll(allTopics);

                LinearLayoutManager linearLayoutManager
                        = new LinearLayoutManager(AllTopicActivity.this);
                recyclerViewAllTopics.setLayoutManager(linearLayoutManager);

                AllTopicsAdapter allTopicsAdapter
                        = new AllTopicsAdapter(AllTopicActivity.this, allTopics);
                recyclerViewAllTopics.setAdapter(allTopicsAdapter);


                //Log.d("bbb", allTopics.get(0).getTopicImagine());
            }

            @Override
            public void onFailure(Call<List<Topic>> call, Throwable t) {
                Log.d("bbb", "Failed at AllTopicActivity.java/ func setDataActivityFromAPI()");
            }
        });
    }
}